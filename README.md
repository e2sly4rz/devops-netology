# devops-netology
devops-netology

В соотвесттвии с описанием файла .gitignore для Terrafrom будут игнорироваться:

- Файлы с расширением начинающемся на .tfstate
- Файлы crash.log, override.tf, override.tf.json, terraform.rc
- Все файлы с расширением _override.tf, _override.tf.json, .tfvars, .terraformrc
